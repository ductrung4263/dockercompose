const express = require('express')
const app = express()
const port = 3000
const mongoose = require('mongoose');
const cors = require('cors');
const dbHost = process.env.DB_HOST || 'localhost'
const dbPort = process.env.DB_PORT || 27017
const dbName = process.env.DB_NAME || 'cicd_automation_test'


app.use(express.urlencoded({ extended: false }));
app.use(express.json());
mongoose.connect(`mongodb://${dbHost}:${dbPort}/${dbName}`, { useNewUrlParser: true });
app.use(cors())

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (e) {
  console.log("Connected to MongoDB using Mongoose!");
});

const users = new mongoose.Schema({
  userName: String,
  userAge: Number
});
const user = mongoose.model('user', users);

app.post('/addUser', async (req, res) => {
  console.log("req body", req.body);
  const instance = new user(req?.body);
  instance.save((error) => {
    if (error) {
      res.send(error);
    } else {
      res.send("Data saved successfully!");
    }
  });
})

app.get('/', async (req, res) => {
  const query = await user.find();
  // query.exec(function(err, people) {
  //     if (err) return console.error(err);
  //     console.log(people);
  //   });
  res.send({ query })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})