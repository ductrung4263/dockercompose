# Use an official Node.js image as the base image
FROM node:15.3.0

# Set the working directory
WORKDIR /app

# Copy the package.json and package-lock.json files
COPY package*.json ./

# Install the Node.js dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the default port for Node.js
EXPOSE 3000

# Start the Node.js app
CMD ["npm", "run","start"]